import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DbService {

  constructor(private http: HttpClient) { }
  getUserList() {
    return this.http.get("http://127.0.0.1:8000/api/user/");
  }
  postUser(user_obj: {}) {
    return this.http.post("http://127.0.0.1:8000/api/user/", user_obj);

  }
  deleteUser(id) {
    return this.http.delete(`http://127.0.0.1:8000/api/user/${id}/`)
  }
  update(id, updated_use_obj) {
    return this.http.put(`http://127.0.0.1:8000/api/user/${id}`, updated_use_obj)
  }
}
