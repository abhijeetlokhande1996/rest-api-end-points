import { Component, OnInit } from '@angular/core';
import { DbService } from './services/db.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'RestApiEndPoints';
  updateErrFlag: boolean = false;
  constructor(private dbService: DbService) { }
  ngOnInit(): void {
    this.dbService.getUserList().subscribe(resp => {
      console.info(resp)
    });
    // const user_obj = {
    //   "name": "MK Patil",
    //   "email": "mk@example.com"
    // }
    // this.dbService.postUser(user_obj).subscribe(resp => {
    //   console.log(resp)
    // })
  }
  delete() {
    this.dbService.deleteUser(2).subscribe(resp => {
      console.log(resp)
    })

  }
  update() {
    const updated_user_obj = {
      "email": "abhijeet@example.com",
      "name": "MK Patil"
    }
    this.dbService.update(6, updated_user_obj).subscribe(
      resp => {
        console.log(resp)
        this.updateErrFlag = false
      }, (err) => {
        this.updateErrFlag = true
        console.log(err)
      })
  }
}
